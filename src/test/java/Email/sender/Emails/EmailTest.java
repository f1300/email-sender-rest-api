package Email.sender.Emails;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmailTest {

    Email email;

    @BeforeEach
    void setUp() {
        email = new Email("user@gmail.com", "user");
    }

    @Test
    void getId() {
        Assertions.assertEquals(null, email.getId());
    }

    @Test
    void setId() {
        email.setId((long) 2);
        Assertions.assertEquals(2, email.getId());
    }

    @Test
    void getMail_address() {
        Assertions.assertEquals("user@gmail.com", email.getMail_address());
    }

    @Test
    void setMail_address() {
        email.setMail_address("user2@gmail.com");
        Assertions.assertEquals("user2@gmail.com", email.getMail_address());
    }

    @Test
    void getUsername() {
        Assertions.assertEquals("user", email.getUsername());
    }

    @Test
    void setUsername() {
        email.setUsername("user2");
        Assertions.assertEquals("user2", email.getUsername());
    }
}
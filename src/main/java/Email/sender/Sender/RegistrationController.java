package Email.sender.Sender;

import Email.sender.Emails.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RegistrationController {

    @Autowired
    private MailSender notificationSender;

    @Autowired
    private Email email;

    @RequestMapping("send-mail")
    public String send() {

        email.setMail_address("addissonpl@gmail.com");

        try {
            notificationSender.sendEmail(email);
        } catch (MailException mailException) {
            System.out.println(mailException);
        }
        return "Congratulations! Your mail has been send to all users.";
    }
}
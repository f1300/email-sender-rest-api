package Email.sender.Emails;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    private final EmailRepository repository;

    EmailController(EmailRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/emails")
    CollectionModel<EntityModel<Email>> all() {

        List<EntityModel<Email>> emails = repository.findAll().stream()
                .map(email -> EntityModel.of(email,
                        linkTo(methodOn(EmailController.class).one(email.getId())).withSelfRel(),
                        linkTo(methodOn(EmailController.class).all()).withRel("emails")))
                .toList();

        return CollectionModel.of(emails, linkTo(methodOn(EmailController.class).all()).withSelfRel());
    }

    @PostMapping("/emails")
    Email newEmail(@RequestBody Email newEmail) {
        return repository.save(newEmail);
    }

    @GetMapping("/emails/{id}")
    EntityModel<Email> one(@PathVariable Long id) {

        Email email = repository.findById(id) //
                .orElseThrow(() -> new EmailNotFoundException(id));

        return EntityModel.of(email, //
                linkTo(methodOn(EmailController.class).one(id)).withSelfRel(),
                linkTo(methodOn(EmailController.class).all()).withRel("employees"));
    }
    @PutMapping("/emails/{id}")
    Email replaceEmail(@RequestBody Email newEmail, @PathVariable Long id) {

        return repository.findById(id)
                .map(email -> {
                    email.setUsername(newEmail.getUsername());
                    email.setMail_address(newEmail.getMail_address());
                    return repository.save(email);
                })
                .orElseGet(() -> {
                    newEmail.setId(id);
                    return repository.save(newEmail);
                });
    }

    @DeleteMapping("/emails/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
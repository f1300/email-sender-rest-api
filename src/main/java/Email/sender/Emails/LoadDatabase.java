package Email.sender.Emails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(EmailRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Email("adrian_nowicki@gmail.com", "adrinowi")));
            log.info("Preloading " + repository.save(new Email("adrianNowicki@outlook.com", "adi12")));
        };
    }
}

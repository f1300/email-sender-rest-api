package Email.sender.Emails;

public class EmailNotFoundException extends RuntimeException{
    EmailNotFoundException(long id){
        super("Could not find email " + id );
    }
}

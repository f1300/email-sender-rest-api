package Email.sender.Emails;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Entity
@Component("emailClass")
public class Email {
    private @Id @GeneratedValue Long id;
    private String mail_address;

    private String username;

    public Email(String mail_address, String username) {
        this.mail_address = mail_address;
        this.username = username;
    }

    public Email() {

    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMail_address() {
        return mail_address;
    }

    public void setMail_address(String mail_address) {
        this.mail_address = mail_address;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Email email)) return false;
        return id.equals(email.id) && mail_address.equals(email.mail_address) && username.equals(email.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, mail_address, username);
    }

    @Override
    public String toString(){
        return "Email{"+ "id=" + this.id + ", username=" + this.username + '\'' + "mail address=" + this.mail_address;
    }

}

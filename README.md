# AUTOMATIC EMAIL SENDER - REST API

(PROJECT) Automatic email sender created using spring boot REST API. Takes all emails stored in the database and sends the information to all mail addresses.


## Starting a Spring App

Use the [spring initializr](https://start.spring.io/) to generate a starting point using following options
#### Project
Maven
#### Spring Boot
3.0.3
#### Java
17

#### Dependencies
[WEB]
[JPA]
[HATEOAS]
[JAVA MAIL SENDER]



## Run
Run the application using EmailSenderApplication placed in
Email-sender/src/main/java/Email/sender/
```java
@SpringBootApplication
public class EmailSenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailSenderApplication.class, args);
	}

}
```
open browser and go to:
```bash
http://localhost:8080/
```

### Objectives of the project (paths)
#### REST API to send emails
```
src/main/java/Email/sender/Sender
```
#### REST API to store emails in data base
```
src/main/java/Email/sender/Emails
```
#### Loggs (overall and response)
```
src/main/java/Email/sender/Logger
src/main/resources/logback.xml
/Email-sender/request.log
/Email-sender/all.log
```

## Comments
Project is incomplete, any suggestions are welcome



